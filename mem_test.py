from memory import test
import numpy as np
from memory_profiler import profile

@profile
def main():
    y = np.ones((512, 512, 3), dtype=np.int32)
    res = test()

if __name__ == '__main__':
    main()
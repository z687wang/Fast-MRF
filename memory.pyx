from libcpp cimport bool
from libc.stdio cimport printf
from libc.stdlib cimport rand
from libc.string cimport memcpy
cimport cython
from cpython.array cimport array, clone
import tensorflow as tf
from cython.parallel import parallel, prange
import numpy as np
cimport numpy as np
ctypedef np.uint8_t uint8

cdef extern from "smmintrin.h":
    ctypedef int __m128i
    __m128i _mm_mullo_epi32 (__m128i __X, __m128i __Y) nogil
    __m128i _mm_min_epi32 (__m128i __X, __m128i __Y) nogil
    __m128i _mm_set1_epi32 (int a) nogil

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void copy(int[:]a, int[:]b ,int iter) nogil:
    cdef int i
    # todo: optimize operation with Intel SSE instruction set
    for i in prange(iter):
        a[iter] = b[iter]

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void copy_i(int[:]a, int b ,int iter) nogil:
    cdef int i
    # todo: optimize operation with Intel SSE instruction set
    for i in prange(iter):
        a[iter] = b


@cython.boundscheck(False)
@cython.wraparound(False)
cdef void add(int[:]a, int[:]b, int iter) nogil:
    cdef int i
    # todo: optimize operation with Intel SSE instruction set
    for i in prange(iter):
        a[iter] += b[iter]

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void add_i(int[:]a, int b, int iter) nogil:
    cdef int i
    # todo: optimize operation with Intel SSE instruction set
    for i in prange(iter):
        a[iter] += b

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void makeMin(int[:]a, int[:]b, int iter) nogil:
    cdef int i
    # todo: optimize operation with Intel SSE instruction set
    for i in prange(iter):
        a[iter] = min(a[iter], b[iter])

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void makeMin_i(int[:] a, int b, int iter) nogil:
    cdef int i
    # todo: optimize operation with Intel SSE instruction set
    for i in prange(iter):
        a[iter] = min(a[iter], b)

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void mul(int[:] a, int[:] b, int c, int iter) nogil:
    cdef int i
    # todo: optimize operation with Intel SSE instruction set
    for i in prange(iter):
        a[iter] = b[iter] * c

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void BF(int[:,:] data, int[:,:] smooth_term, int length, int[:] out, int[:] weights, int labels,  int radius, int l4, int[:,:] f, int[:] left, int[:] right, int[:] recx) nogil:
    cdef int rec, i, j, k, next_i = 0
    cdef int min_val = 0
    cdef int tw = smooth_term[0][labels-1]
    f[0] = data[0]
    for i in prange(length-1):
        f[i] = f[i-1]
        for j in prange(radius-1):
            copy(left, f[i-1, j:], labels-j)
            copy(right[j:], f[i-1], labels-j)
            copy_i(left[labels-j:], 63, j)
            copy_i(right, 63, j)
            makeMin(left, right, l4)
            add_i(left, smooth_term[0, j] * weights[i-1], l4)
            makeMin(f[i], left, l4)
        rec = 0
        min_val = f[i-1][rec]
        for k in range(1, labels-1):
            if (f[i-1, k] < min_val):
                min_val = f[i-1, k]
                rec = k
        recx[i] = rec
        makeMin_i(f[i], min_val + tw * weights[i-1], l4)
        add(f[i], data[i], l4)
    rec = 0
    for k in range(1, labels-1):
        if (f[length-1, k] < f[length-1, rec]):
            rec = k
    for i in range(length-1, 0):
        out[i] = rec
        next_i = recx[i]
        for j in range(rec-radius+1, rec+radius-1):
            if ((j >= 0) and (j < labels) and (f[i, rec] == f[i-1, j] + smooth_term[rec, j] * weights[i-1] + data[i, rec])):
                next_i = j
                break
        rec = next_i
    out[0] = rec



@cython.boundscheck(False)
@cython.wraparound(False)
cdef int L1(int[:,:] data, int length, int[:] out, int[:] weights, int labels) nogil:
    # todo
    return 0

@cython.boundscheck(False)
@cython.wraparound(False)
cdef int L2(int[:,:] data, int length, int[:] out, int[:] weights, int labels) nogil:
    # todo
    return 0


class FastMRFLayer(tf.keras.layers.Layer):
    def __init__(self, reg_weight=10, penalty_type=1, truncate=5, max_iter=100):
        super(FastMRFLayer, self).__init__()
        self.reg_weight = reg_weight
        self.penalty_type = penalty_type
        self.truncate = truncate
        self.max_iter = max_iter
        self.sess = tf.compat.v1.Session()
        self.weighted = True
        self.RADIUS = 4
    
    @cython.boundscheck(False)
    @cython.wraparound(False)
    def smooth_term_init(self):
        cdef int i, j, diff, l, penalty_type, truncate
        cdef int[:,:] smooth_term = self.smooth_term
        l = self.labels
        truncate = self.truncate
        penalty_type = self.penalty_type
        for i in prange(l, nogil=True):
            for j in prange(l):
                if (i > j):
                    diff = i - j
                else:
                    diff = j - i
                if (penalty_type == 1):
                    smooth_term[i, j] = smooth_term[j, i] = min(diff, truncate)
                else:
                    smooth_term[i, j] = smooth_term[j, i] = min(diff * diff, truncate)
        while self.radius > 0 and smooth_term[0, l-1] == smooth_term[0, self.radius-1]:
             self.radius -= 1
        self.tw = smooth_term[0, l-1]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def optimizeRow(self, int[:,:] cur, bool init, uint8[:] flag):
        cdef int[:,:] data = np.zeros((self.height, self.labels), dtype=np.int32)
        cdef int[:,:] smooth_term = self.smooth_term, smooth_term_unweighted = self.smooth_term_unweighted
        cdef int[:,:] wc = self.col_weights, wr = self.row_weights, wr_unweighted = self.row_weights_unweighted
        cdef int[:,:,:] data_term = self.data_term
        cdef int[:] out = np.zeros((self.height), dtype=np.int32), temp = np.zeros((self.labels), dtype=np.int32)
        cdef bool weighted = self.weighted
        cdef int iter_row = self.width/2, iter_col = self.height, l4 = self.l4, iter_odd = 1
        cdef int radius = self.radius, RADIUS = self.RADIUS, penalty_type = self.penalty_type, labels = self.labels
        cdef int i, j, l, row
        cdef int[:] left = np.zeros((self.labels), dtype=np.int32), right = np.zeros((self.labels), dtype=np.int32)
        cdef int[:] recx = np.zeros((iter_col), dtype=np.int32)
        cdef int[:,:] f = np.zeros((iter_col, self.labels), dtype=np.int32)
        for i in range(iter_odd):
            for j in prange(iter_row, nogil=True):
                row = 2 * j + i
                if (flag[row]):
                    if (weighted):
                        for l in range(iter_col):
                            data[l] = data_term[row, l]
                            if (not init):
                                if (row > 0):
                                    mul(temp, smooth_term[cur[row-1, l]], wc[row-1, l], l4)
                                    add(data[l], temp, l4)
                                if (row < iter_row*2-1):
                                    mul(temp, smooth_term[cur[row+1, l]], wc[row, l], l4)
                                    add(data[l], temp, l4)
                    else:
                        for l in range(iter_col):
                            data[j] = data_term[row, j]
                            if (not init):
                                if (row > 0):
                                    add(data[l], smooth_term_unweighted[cur[row-1, l]], l4)
                                if (row < iter_row*2-1):
                                    add(data[l], smooth_term_unweighted[cur[row+1, l]], l4)
                    
                    if (radius <= RADIUS):
                        BF(data, smooth_term, iter_col, out, wr[row], labels, radius, l4, f, left, right, recx)
                    else:
                        if (penalty_type is 1):
                            L1(data, iter_col, out, wr_unweighted[row], labels)
                        else:
                            L2(data, iter_col, out, wr_unweighted[row], labels)
                    # cur[row] = out

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def optimizeCol(self, int[:,:] cur, bool init, bool[:] flag):
        cdef int[:,:] data = np.zeros((self.height, self.labels), dtype=np.int32)
        cdef int[:,:] smooth_term = self.smooth_term, smooth_term_unweighted = self.smooth_term_unweighted
        cdef int[:, :] wc = self.col_weights, wc_unweighted = self.col_weights_unweighted, wr = self.row_weights
        cdef int[:,:,:] data_term = self.data_term
        cdef int[:] out = np.zeros((self.height), dtype=np.int32), temp = np.zeros((self.labels), dtype=np.int32)
        cdef bool weighted = self.weighted
        cdef int iter_odd = 1, iter_row = self.width, iter_col = self.height/2, l4 = self.l4
        cdef int radius = self.radius, RADIUS = self.RADIUS, penalty_type = self.penalty_type, labels = self.labels
        cdef int i, j, l, col
        cdef int[:] left = np.zeros((self.labels), dtype=np.int32), right = np.zeros((self.labels), dtype=np.int32)
        cdef int[:] recx = np.zeros((iter_row), dtype=np.int32)
        cdef int[:,:] f = np.zeros((iter_row, self.labels), dtype=np.int32)
        for i in range(iter_odd):
            for j in prange(iter_col, nogil=True):
                col = 2 * j + i
                if (flag[col]):
                    if (weighted):
                        for l in range(iter_row):
                            data[l] = data_term[l, col]
                            if (not init):
                                if (col > 0):
                                    mul(temp, smooth_term[cur[l, col-1]], wc[l, col-1], l4)
                                    add(data[l], temp, l4)
                                if (col < iter_col*2-1):
                                    mul(temp, smooth_term[cur[l, col+1]], wc[l, col], l4)
                                    add(data[l], temp, l4)
                    else:
                        for l in range(iter_col):
                            data[l] = data_term[l, col]
                            if (not init):
                                if (col > 0):
                                    add(data[l], smooth_term_unweighted[cur[l, col-1]], l4)
                                if (col < iter_col*2-1):
                                    add(data[l], smooth_term_unweighted[cur[l, col+1]], l4)
                    if (radius <= RADIUS):
                        BF(data, smooth_term, iter_row, out, wc[col], labels, radius, l4, f, left, right, recx)
                    else:
                        if (penalty_type is 1):
                            L1(data, iter_row, out, wc_unweighted[col], labels)
                        else:
                            L2(data, iter_row, out, wc_unweighted[col], labels)
                    for l in prange(iter_row):
                        1+1
                        # cur[l, col] = out[l]

    def build(self, input_shape):
        if len(input_shape) <= 1:
            raise Exception('FastMRFLayer must be called on a list of tensors.' 'Got: ' + str(len(input_shape)))
        self.width, self.height, self.labels = input_shape[0]
        self.row_weights = np.ones((self.width, self.height), dtype=np.int32)
        self.row_weights_unweighted = np.ones((self.width, self.height), dtype=np.int32)
        self.col_weights = np.ones((self.width, self.height), dtype=np.int32)
        self.col_weights_unweighted = np.ones((self.width, self.height), dtype=np.int32)
        self.col2_weights = np.ones((self.height, self.width), dtype=np.int32)
        self.smooth_term = np.zeros((self.labels, self.labels), dtype=np.int32)
        self.smooth_term_unweighted = np.zeros((self.labels, self.labels), dtype=np.int32)
        self.radius = self.labels - 1
        self.l4 = self.labels/4 - 1
        self.smooth_term_init()
        print("data term dimension:", str(self.width), str(self.height), str(self.labels))

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def call(self, inputs):
        if type(inputs) is not list or len(inputs) <= 1:
            raise Exception('FastMRFLayer must be called on a list of tensors.' 'Got: ' + str(len(inputs)))
        self.data_term = inputs[0].numpy()
        self.label_term = inputs[1].numpy()
        self.cur = np.zeros((self.width, self.height), dtype=np.int32)
        self.cur2 = np.zeros((self.width, self.height), dtype=np.int32)
        self.curRow = np.zeros((self.width, self.height), dtype=np.int32)
        self.curCol = np.zeros((self.width, self.height), dtype=np.int32)
        self.rowFlag = np.ones((self.width,), dtype=np.uint8)
        self.colFlag = np.ones((self.height,), dtype=np.uint8)
        self.rowFlag_2 = np.zeros((self.width,), dtype=np.uint8)
        self.colFlag_2 = np.zeros((self.height,), dtype=np.uint8)
        # Optimize all rows independently
        print("optimizeRow\n")
        self.optimizeRow(self.curRow, True, self.rowFlag)
        print("optimizeCol\n")
        # Optimize all cols independently
        self.optimizeCol(self.curCol, True, self.colFlag)
        cdef int i, j 
        cdef int row_iter = self.width-1
        cdef int col_iter = self.height-1
        print(row_iter, col_iter)
        cdef int[:,:] cur = self.cur, curRow = self.curRow, curCol = self.curCol, cur2 = self.cur2
        cdef char[:] rowFlag = self.rowFlag, colFlag = self.colFlag, rowFlag_2 = self.rowFlag_2, colFlag_2 = self.colFlag_2
        for i in prange(row_iter, nogil=True):
            for j in prange(col_iter):
                if (rand() & 1):
                    cur[i, j] = curRow[i, j]
                else:
                    cur[i, j] = curCol[i, j]
        cdef int max_iter = self.max_iter
        for i in range(max_iter):
            print("iteration: ", i)
            cur2[i] = cur[i]
            self.optimizeRow(cur, False, rowFlag)
            self.optimizeCol(cur, False, colFlag)
            rowFlag_2 = rowFlag
            colFlag_2 = colFlag
            for i in prange(row_iter+1, nogil=True):
                rowFlag[i] = 0
            for i in prange(col_iter+1, nogil=True):
                colFlag[i] = 0

            for i in prange(row_iter, nogil=True):
                if (rowFlag_2[i] == <int>1):
                    for j in prange(col_iter):
                        if (cur[i, j] != cur2[i, j]):
                            if (i > 0):
                                rowFlag[i-1] = 1
                            if (i < row_iter):
                                rowFlag[i+1] = 1
            for i in prange(col_iter, nogil=True):
                if (colFlag_2[i] == <int>1):
                    for j in prange(row_iter):
                        if (cur[i, j] != cur2[i, j]):
                            if (j > 0):
                                colFlag[j-1] = 1
                            if (i < col_iter):
                                colFlag[j+1] = 1
        return tf.Variable(self.cur)

        

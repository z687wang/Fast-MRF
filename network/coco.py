import os
import torch
import time
import numpy as np
import scipy
import math
import random
import argparse
import skimage.color
import skimage.io
import skimage.transform
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils
from config import Config
import utils as coco_utils

class CoCoDataset(Dataset):
    def __init__(self, dataset_dir, subset, class_ids=None, year=2017):
        self._image_ids = []
        self.image_info = []
        self.config = Config
        self.class_info = [{"source": "", "id": 0, "name": "BG"}]
        self.source_class_ids = {}
        self.class_ids = class_ids
        self.class_names = ["background", "person"]
        self.num_classese = len(self.class_ids)
        coco = COCO('{}/annotations/instances_{}{}.json'.format(dataset_dir, subset, year))
        image_dir = "{}/{}{}".format(dataset_dir, subset, year)
        
        if not class_ids:
            class_ids = sorted(coco.getCatIds())
        if class_ids:
            image_ids = []
            for id in class_ids:
                image_ids.extend(list(coco.getImgIds(catIds=[id])))
            image_ids = list(set(image_ids))
        else:
            image_ids = list(coco.imgs.keys())
        
        for i in image_ids:
            self.add_image("coco", 
                image_id=i, 
                path=os.path.join(image_dir, coco.imgs[i]['file_name']),
                width=coco.imgs[i]["width"],
                height=coco.imgs[i]["height"],
                annotations=coco.loadAnns(coco.getAnnIds(imgIds=[i], catIds=class_ids, iscrowd=None)))
        self.prepare()

    def add_class(self, source, class_id, class_name):
        assert "." not in source, "Source name cannot contain a dot"
        for info in self.class_info:
            if info['source'] == source and info["id"] == class_id:
                return
        self.class_info.append({
            "source": source,
            "id": class_id,
            "name": class_name,
        })

    def add_image(self, source, image_id, path, **kwargs):
        image_info = {
            "id": image_id,
            "source": source,
            "path": path,
        }
        image_info.update(kwargs)
        self.image_info.append(image_info)
    
    def prepare(self, class_map=None):
        self.num_images = len(self.image_info)
        self._image_ids = np.arange(self.num_images)

    @property
    def image_ids(self):
        return self._image_ids

    def load_image(self, image_id):
        # Load image
        image = skimage.io.imread(self.image_info[image_id]['path'])
        # If grayscale. Convert to RGB for consistency.
        if image.ndim != 3:
            image = skimage.color.gray2rgb(image)
        # If has an alpha channel, remove it for consistency
        if image.shape[-1] == 4:
            image = image[..., :3]
        return image

    def load_mask(self, image_id):
        image_info = self.image_info[image_id]
        class_masks = []
        class_mask_map = {}
        annotations = self.image_info[image_id]["annotations"]
        class_count = 0
        count = 0
        
        for class_id in self.class_ids:
            m = np.zeros([image_info["height"], image_info["width"]], dtype=bool)
            class_masks.append(m)
            class_mask_map[class_id] = class_count
            class_count += 1
        
        for annotation in annotations:
            class_id  = annotation['category_id']
            if class_id in self.class_ids:
                count += 1
                m = self.annToMask(annotation, image_info["height"], image_info["width"])
                o_mask = class_masks[class_mask_map[class_id]]
                class_masks[class_mask_map[class_id]] = np.logical_or(o_mask, m)
                if m.max() < 1:
                    continue

        if self.class_ids:
            mask = np.stack(class_masks, axis=2).astype(np.bool)
            self.class_ids = np.array(self.class_ids, dtype=np.int32)
            return mask, self.class_ids
        else:
            return super(CoCoDataset, self).load_mask(image_id)
    
    def load_image_gt(self, image_id):
        # Load image and mask off 
        image = self.load_image(image_id)
        mask, class_ids = self.load_mask(image_id)
        original_shape = image.shape
        image, window, scale, padding, crop = coco_utils.resize_image(
            image,
            min_dim=self.config.IMAGE_MIN_DIM,
            min_scale=self.config.IMAGE_MIN_SCALE,
            max_dim=self.config.IMAGE_MAX_DIM,
            mode=self.config.IMAGE_RESIZE_MODE)
        mask = coco_utils.resize_mask(mask, scale, padding, crop)
        return image, class_ids, mask
    
    def __len__(self):
        return len(self._image_ids)

    def __getitem__(self, idx):
        image_id = self._image_ids[idx]
        image, gt_class_id, gt_masks = self.load_image_gt(image_id)
        image = np.rollaxis(image, 2, 0)
        gt_masks = np.rollaxis(gt_masks, 2, 0)
        return image, gt_masks

    def annoToRLE(self, ann, height, width):
        segm = ann['segmentation']
        if isinstance(segm, list):
            rles = maskUtils.frPyObjects(segm, height, width)
            rle = maskUtils.merge(rles)
        elif isinstance(segm['counts'], list):
            rle = maskUtils.frPyObjects(segm, height, width)
        else:
            rle = ann['segmentation']
        return rle
    
    def annToMask(self, ann, height, width):
        rle = self.annoToRLE(ann, height, width)
        m = maskUtils.decode(rle)
        return m

def build_coco_results(dataset, image_ids, class_ids, scores, masks):
    results = []
    for image_id in image_ids:
        for i in range(scores.shape[0]):
            class_id = class_ids[i]
            score = scores[i]
            mask = masks[:, :, i]
            result = {
                "image_id": image_id,
                "category_id": dataset.get_source_class_id(class_id, "coco"),
                "score": score,
                "segmentation": maskUtils.encode(np.asfortranarray(mask))
            }
            results.append(result)
    return results

def evaluate_coco(model, dataset, coco, limit=0, image_ids=None):
    if limit:
        image_ids = image_ids[:limit]
    coco_image_ids = [dataset.image_info[id]["id"] for id in image_ids]
    t_prediction = 0
    t_start = time.time()
    results = []
    for i, image_id in enumerate(image_ids):
        image = dataset.load_image(image_id)
        t = time.time()
        r = model.detect([image], verbose=0)[0]
        t_prediction += (time.time() - t)
        image_results = build_coco_results(dataset, coco_image_ids[i:i+1], r["class_ids"], r["scores"], r["masks"].astype(np.uint8))
        results.extend(image_results)
    
    coco_results = coco.loadRes(results)

    cocoEval = COCOeval(coco, coco_results, "segm")
    cocoEval.params.imgIds = coco_image_ids
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()

    print("Prediction time: {}. Average {}/image".format(
        t_prediction, t_prediction / len(image_ids)))
    print("Total time: ", time.time() - t_start)

def iou_metrics(gt, pred, batch_size):
    iou_score = 0
    for i in range(batch_size):
        intersection = np.logical_and(gt[i], pred[i])
        union = np.logical_or(gt[i], pred[i])
        iou_score += np.sum(intersection) / np.sum(union)
    iou_score /= batch_size
    return iou_score


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN on MS COCO.')
    parser.add_argument("--mode",
                        default="train",
                        metavar="<mode>",
                        help="'train' or 'test' on COCO")
    parser.add_argument('--dataset', required=False,
                        default='./FCN.tensorflow/coco/',
                        metavar="/path/to/coco/",
                        help='Directory of the COCO dataset')
    parser.add_argument('--logs_dir', required=False,
                        default='logs/',
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    
    parser.add_argument('--model_dir', required=False,
                        default='Model_zoo/',
                        metavar="/path/to/models/",
                        help='models directory (default=Model_zoo/)')

    parser.add_argument('--limit', required=False,
                        default=500,
                        metavar="<image count>",
                        help='Images to use for evaluation (default=500)')
    args = parser.parse_args()
    print("Mode: ", args.mode)
    print("Dataset: ", args.dataset)
    print("Logs: ", args.logs_dir)
    dataset = CoCoDataset(args.dataset, "train", [1])
    data_loader = DataLoader(dataset, batch_size=32, shuffle=True, num_workers=4)

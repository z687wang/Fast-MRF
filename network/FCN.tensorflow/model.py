import numpy as np
import utils
import datetime
import time
import tensorflow as tf
from FastMRFLayer import FastMRFLayer
from tensorflow import keras
from tensorflow.keras import datasets, layers, optimizers, models
from tensorflow.keras import regularizers
import TensorflowUtils as tf_utils
import datetime
from six.moves import xrange


def vgg19_mrf(config, keep_prob=0.85, NUM_OF_CLASSESS=2, output_channels=3):
    keep_prob = 0.5
    NUM_OF_CLASSESS = 2
    output_channels = 3
    vgg_model = tf.keras.applications.vgg19.VGG19(include_top=False, weights='imagenet', input_tensor=None, input_shape=None, pooling=None, classes=1000)
    x = layers.Conv2D(4096, (7, 7), padding='same', name='conv6')(tf.cast(vgg_model.output, tf.float64))
    x = layers.Activation('relu', name='relu6')(x)
    x = layers.Dropout(keep_prob, name='dropout6')(x)
    x = layers.Conv2D(4096, 1, 1, name='conv7')(x)
    x = layers.Activation('relu', name='relu7')(x)
    x = layers.Dropout(keep_prob, name='dropout7')(x)
    x = layers.Conv2D(NUM_OF_CLASSESS, 1, 1, name='conv8')(x)
    block4_pool = vgg_model.get_layer('block4_pool')
    x = layers.Conv2DTranspose(block4_pool.output_shape[3], kernel_size=(4, 4), strides=(2, 2), padding='same', name='deconv9')(x)
    x = layers.Add(name='fusion1')([tf.cast(x, tf.float64), tf.cast(block4_pool.output, tf.float64)])
    block3_pool = vgg_model.get_layer('block3_pool')
    x = layers.Conv2DTranspose(block3_pool.output_shape[3], kernel_size=(4, 4), strides=(2, 2), padding='same', name='deconv10')(tf.cast(x, tf.float64))
    x = layers.Add(name='fusion2')([tf.cast(x, tf.float64), tf.cast(block3_pool.output, tf.float64)])
    outputs = layers.Conv2DTranspose(NUM_OF_CLASSESS, kernel_size=(16, 16), strides=(8, 8), padding='same', name='deconv11')(tf.cast(x, tf.float64))
    model = tf.keras.models.Model(inputs=vgg_model.input, outputs=outputs)
    for layer in vgg_model.layers:
        layer.trainable = False
    return model

def vgg19(keep_prob=0.85, NUM_OF_CLASSESS=2, output_channels=3):
    keep_prob = 0.5
    NUM_OF_CLASSESS = 2
    output_channels = 3
    vgg_model = tf.keras.applications.vgg19.VGG19(include_top=False, weights='imagenet', input_tensor=None, input_shape=None, pooling=None, classes=1000)
    x = layers.Conv2D(4096, (7, 7), padding='same', name='conv6')(tf.cast(vgg_model.output, tf.float64))
    x = layers.Activation('relu', name='relu6')(x)
    x = layers.Dropout(keep_prob, name='dropout6')(x)
    x = layers.Conv2D(4096, 1, 1, name='conv7')(x)
    x = layers.Activation('relu', name='relu7')(x)
    x = layers.Dropout(keep_prob, name='dropout7')(x)
    x = layers.Conv2D(NUM_OF_CLASSESS, 1, 1, name='conv8')(x)
    block4_pool = vgg_model.get_layer('block4_pool')
    x = layers.Conv2DTranspose(block4_pool.output_shape[3], kernel_size=(4, 4), strides=(2, 2), padding='same', name='deconv9')(x)
    x = layers.Add(name='fusion1')([tf.cast(x, tf.float64), tf.cast(block4_pool.output, tf.float64)])
    block3_pool = vgg_model.get_layer('block3_pool')
    x = layers.Conv2DTranspose(block3_pool.output_shape[3], kernel_size=(4, 4), strides=(2, 2), padding='same', name='deconv10')(tf.cast(x, tf.float64))
    x = layers.Add(name='fusion2')([tf.cast(x, tf.float64), tf.cast(block3_pool.output, tf.float64)])
    outputs = layers.Conv2DTranspose(NUM_OF_CLASSESS, kernel_size=(16, 16), strides=(8, 8), padding='same', name='deconv11')(tf.cast(x, tf.float64))
    model = tf.keras.models.Model(inputs=vgg_model.input, outputs=outputs)
    for layer in vgg_model.layers:
        layer.trainable = False
    return model

def vgg19_no_fusion(config, keep_prob=0.85, NUM_OF_CLASSESS=2, output_channels=3):
    keep_prob = 0.5
    NUM_OF_CLASSESS = 2
    output_channels = 3
    vgg_model = tf.keras.applications.vgg19.VGG19(include_top=False, weights='imagenet', input_tensor=None, input_shape=None, pooling=None, classes=1000)
    x = layers.Conv2D(4096, (7, 7), padding='same', name='conv6')(tf.cast(vgg_model.output, tf.float64))
    x = layers.Activation('relu', name='relu6')(x)
    x = layers.Dropout(keep_prob, name='dropout6')(x)
    x = layers.Conv2D(4096, 1, 1, name='conv7')(x)
    x = layers.Activation('relu', name='relu7')(x)
    x = layers.Dropout(keep_prob, name='dropout7')(x)
    x = layers.Conv2D(NUM_OF_CLASSESS, 1, 1, name='conv8')(x)
    block4_pool = vgg_model.get_layer('block4_pool')
    x = layers.Conv2DTranspose(block4_pool.output_shape[3], kernel_size=(4, 4), strides=(2, 2), padding='same', name='deconv9')(x)
    # x = layers.Add(name='fusion1')([tf.cast(x, tf.float64), tf.cast(block4_pool.output, tf.float64)])
    block3_pool = vgg_model.get_layer('block3_pool')
    x = layers.Conv2DTranspose(block3_pool.output_shape[3], kernel_size=(4, 4), strides=(2, 2), padding='same', name='deconv10')(tf.cast(x, tf.float64))
    # x = layers.Add(name='fusion2')([tf.cast(x, tf.float64), tf.cast(block3_pool.output, tf.float64)])
    outputs = layers.Conv2DTranspose(NUM_OF_CLASSESS, kernel_size=(16, 16), strides=(8, 8), padding='same', name='deconv11')(tf.cast(x, tf.float64))
    model = tf.keras.models.Model(inputs=vgg_model.input, outputs=outputs)
    for layer in vgg_model.layers:
        layer.trainable = False
    return model



def mold_images(images, config):
    return images.astype(np.float32)

def unmold_images(normalized_images, config):
    #  + config.MEAN_PIXEL
    return (normalized_images).astype(np.uint8)

def load_image_gt(dataset, config, image_id, augment=False, augmentation=None,
                  use_mini_mask=False):
    # Load image and mask
    image = dataset.load_image(image_id)
    mask, class_ids = dataset.load_mask(image_id)
    original_shape = image.shape
    image, window, scale, padding, crop = utils.resize_image(
        image,
        min_dim=config.IMAGE_MIN_DIM,
        min_scale=config.IMAGE_MIN_SCALE,
        max_dim=config.IMAGE_MAX_DIM,
        mode=config.IMAGE_RESIZE_MODE)
    mask = utils.resize_mask(mask, scale, padding, crop)
    return image, class_ids, mask

def data_generator(dataset, config, shuffle=True, augment=False, augmentation=None):
    b = 0
    image_index = -1
    batch_size = config.BATCH_SIZE
    image_ids = np.copy(dataset.image_ids)
    error_count = 0
    while True:
        try:
            image_index = (image_index + 1) % len(image_ids)
            
            if shuffle and image_index == 0:
                np.random.shuffle(image_ids)
            
            image_id = image_ids[image_index]
            image, gt_class_ids, gt_masks = load_image_gt(dataset, 
                config, image_id, augment=augment, augmentation=augmentation)

            if b == 0:
                batch_images = np.zeros(
                    (batch_size,) + image.shape, dtype=np.float32)
                batch_gt_class_ids = np.zeros(
                    (batch_size, dataset.num_classes), dtype=np.int32)
                batch_gt_masks = np.zeros(
                    (batch_size, gt_masks.shape[0], gt_masks.shape[1], dataset.num_classes),
                    dtype=np.int32)
            
            batch_images[b] = mold_images(image.astype(np.float32), config)
            batch_gt_class_ids[b, :gt_class_ids.shape[0]] = gt_class_ids
            batch_gt_masks[b, :, :, :gt_masks.shape[-1]] = gt_masks
            b += 1
            if b >= batch_size:
                yield batch_images, batch_gt_class_ids, batch_gt_masks
                b = 0
        except:
            raise

class DataGenerator(keras.utils.Sequence):
    def __init__(self, dataset, config, shuffle=True):
        self.image_index = -1
        b = 0
        self.batch_size = config.BATCH_SIZE
        self.image_ids = np.copy(dataset.image_ids)

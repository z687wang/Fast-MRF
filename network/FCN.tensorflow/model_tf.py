from FastMRFLayer import FastMRFLayer
import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
import tensorflow.keras.backend as K



class Model(object):
    def __init__(self):
        self.dense_1 = keras.layers.Dense(32, activation='relu')
        self.dense_2 = keras.layers.Dense(32, activation='relu')
        self.mrf = FastMRFLayer(10, 2, 100)
        self.trainable_variables = self.dense_1.trainable_variables

    def forward(self, X):
        y = self.compute_output(X)
        return y
  
    def loss(self, y_pred, y_true):
        # Cast y_pred to float32
        y_pred = tf.cast(y_pred, dtype=tf.float32)
        return tf.losses.mean_squared_error(y_true, y_pred)
  
    def backward(self, X_train, y_train):
        self.trainable_variables = self.dense_1.trainable_variables + self.dense_2.trainable_variables
        optimizer = tf.keras.optimizers.Adam()
        with tf.GradientTape() as tape:
            predicted = self.forward(X_train)
            current_loss = self.loss(predicted, y_train)
            grads = tape.gradient(current_loss, self.trainable_variables)
            optimizer.apply_gradients(zip(grads, self.trainable_variables))
            print("loss:", current_loss[0])
        
        
    def compute_output(self, X):
        X = tf.cast(X, dtype=tf.float32)
        output = self.dense_1(X)
        output = self.dense_2(output)
        tf.print(output)
        output = self.mrf(output)
        tf.print(output)
        return output

# model = tf.keras.models.Model(inputs=[input_left, input_right], outputs=output)
# model.run_eagerly = True
# model.compile(optimizer='sgd',
#               loss='mean_squared_error',
#               metrics=['accuracy'])
# # model.summary()

# optimizer = tf.keras.optimizers.Adam()

# def train_step(x, y):
#     with tf.GradientTape() as tape:
#         logits = model(x, training=True)     
#         loss_value = tf.keras.losses.MSE(y_true=y, y_pred=logits)
#         grads = tape.gradient(loss_value, model.trainable_variables)
#         optimizer.apply_gradients(zip(grads, model.trainable_variables))
#         return loss_value
model = Model()
for i in range(100):
    model.backward(np.ones((3, 16)), np.zeros((3, 32)))

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils
import numpy as np
import time

dataDir = './coco'

annFile = '{}/annotations/instances_{}.json'.format(dataDir, dataType)

coco = COCO(annFile)

cats = coco.loadCats(coco.getCatIds())
nms = [cat['name'] for cat in cats]
print("COCO categories: \n{}\n".format(''.join(nms)))
catIds = coco.getCatIds(catNms=['person'])
imgIds = coco.getImgIds(catIds=catIds)
img = coco.loadImgs(imgIds[np.random.randint(0, len(imgIds))])[0]
annIds = coco.getAnnIds(imgIds=img['id'], catIds=catIds, iscrowd=None)
anns = coco.loadAnns(annIds)
print(anns)

class CoCoConfig(Config):
    NAME = "coco"
    IMAGES_PER_GPU = 2
    NUM_CLASSES = 2

class CoCoDataset(utils.Dataset):
    def load_coco(self, dataset_dir, subset, class_ids=None, return_coco=False, year=2017):
        coco = COCO('{}/annotations/instances_{}{}.json'.format(dataset_dir, subset, year))
        image_dir = "{}/{}{}".format(dataset_dir, subset, year)
        if not class_ids:
            class_ids = sorted(coco.getCatIds())
        if class_ids:
            image_ids = []
            for id in class_ids:
                image_ids.extend(list(coco.getImgIds(catIds=[id])))
            image_ids = list(set(image_ids))
        else:
            image_ids = list(coco.imgs.keys())
        for i in image_ids:
            self.add_image("coco", 
                image_id=i, 
                path=os.path.join(image_dir, coco.imgs[i]['filename']),
                width=coco.imgs[i]["width"],
                height=coco.imgs[i]["height"],
                annotations=coco.loadAnns(coco.getAnnIds(imgIds=[i], catIds=class_ids, iscrowd=None)))
        if return_coco:
            return coco
    
    def load_mask(self, image_id):
        image_info = self.image_info[image_id]
        instance_masks = []
        class_ids = []
        annotations = self.image_info[image_id]["annotations"]
        for annotation in annotations:
            class_id = self.map_source_class_id(
                "coco.{}".format(annotation['category_id'])
            )
            if class_id:
                m = self.annToMask(annotation, image_info["height"], image_info["width"])
                if m.max() < 1:
                    continue
                if annotation['iscrowd']:
                    class_id *= -1
                    if m.shape[0] != image_info["height"] or m.shape[1] != image_info["width"]:
                        m = np.ones([image_info["height"], image_info["width"]], dtype=bool)
                instance_masks.append(m)
                class_ids.append(class_id)
            if class_ids:
                mask = np.stack(instance_masks, axis=2).astype(np.bool)
                class_ids = np.array(class_ids, dtype=np.int32)
                return mask, class_ids
            else:
                return super(CoCoDataset, self).load_mask(image_id)
    
    def annoToRLE(self, ann, height, width):
        segm = ann['segmentation']
        if isinstance(segm, list):
            rles = maskUtils.frPyObjects(segm, height, width)
            rle = maskUtils.MERGE(rles)
        elif isinstance(segm['counts'], list):
            rle = maskUtils.frPyObjects(segm, height, width)
        else:
            rle = ann['segmentation']
        return rle
    
    def annToMask(self, ann, height, width):
        rle = self.annoToRLE(ann, height, width)
        m = maskUtils.decode(rle)
        return m

def build_coco_results(dataset, image_ids, rois, class_ids, scores, masks):
    if rois is None:
        return []
    results = []
    for image_id in image_ids:
        for i in range(rois.shape[0]):
            class_id = class_ids[i]
            score = scores[i]
            bbox = np.around(rois[i], 1)
            mask = masks[:, :, i]
            result = {
                "image_id": image_id,
                "category_id": dataset.get_source_class_id(class_id, "coco"),
                "bbox": [bbox[1], bbox[0], bbox[3] - bbox[1], bbox[2] - bbox[0]],
                "score": score,
                "segmentation": maskUtils.encode(np.asfortranarray(mask))
            }
            results.append(result)
    return results

def evaluate_coco(model, dataset, coco, limit=0, image_ids=None):
    if limit:
        image_ids = image_ids[:limit]
    coco_image_ids = [dataset.image_info[id]["id"] for id in image_ids]
    t_prediction = 0
    t_start = time.time()
    results = []
    for i, image_id in enumerate(image_ids):
        image = dataset.load_image(image_id)
        t = time.time()
        r = model.detect([image], verbose=0)[0]
        t_prediction += (time.time() - t)
        image_results = build_coco_results(dataset, coco_image_ids[i:i+1], 
            r["rois"], r["class_ids"], r["scores"], r["masks"].astype(np.uint8))

import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
import tensorflow.keras.backend as K

def copy(a, b , iter):
    for i in range(iter):
        a[iter] = b[iter]

def copy_i(a, b , iter):
    for i in range(iter):
        a[iter] = b

def add(a, b, iter):
    for i in range(iter):
        a[iter] += b[iter]

def add_i(a, b, iter):
    for i in range(iter):
        a[iter] += b

def makeMin(a, b, iter):
    for i in range(iter):
        a[iter] = min(a[iter], b[iter])

def makeMin_i(a, b, iter):
    for i in range(iter):
        a[iter] = min(a[iter], b)

def mul(a, b, c, iter):
    for i in range(iter):
        a[iter] = b[iter] * c

class FastMRFLayer(tf.keras.layers.Layer):
    def __init__(self, mode="row", reg_weight=10, penalty_type=1, **kwargs):
        super(FastMRFLayer, self).__init__(**kwargs)
        self.mode = mode
        self.reg_weight = reg_weight
        self.penalty_type = penalty_type
        self.weighted = True
        self.prev_tensor = None

    def smooth_term_init(self):
        l = self.labels
        for i in range(l):
            for j in range(l):
                if (i > j):
                    diff = i - j
                else:
                    diff = j - i
                self.smooth_term[i, j] = self.smooth_term[j, i] = diff

    def energy(self, cur):
        sum_energy, row_energy, col_energy = 0
        row_iter = self.width
        col_iter = self.height
        for i in range(row_iter):
            for j in range(col_iter):
                if (i == row_iter):
                    row_energy = 0
                else:
                    row_energy = self.col_weights[i, j] * self.smooth_term[cur[i, j], cur[i + 1, j]]
                if (j == col_iter):
                    col_energy = 0
                else:
                    col_energy = self.row_weights[i, j] * self.smooth_term[cur[i, j], cur[i, j + 1]]
                sum_energy += data_term[i, j, cur[i, j]] + row_energy + col_energy
        return sum_energy

    def Find_Min(self, data, length, out, weights, labels, radius, f, recx):
        rec, i, j, k, next_i, min_val = 0
        f[0] = data[0]
        for i in range(1, length):
            f[i] = f[i-1]
            for j in range(0, labels):
                min_v = f[i-1, 0] + self.smooth_term[j][0] * weights[i-1]
                for k in range(1, labels):
                    cur_v = f[i-1, k] + self.smooth_term[j][k] * weights[i-1]
                    if cur_v <= min_v:
                        min_v = cur_v
                f[i, j] = min_v
            rec = 0
            min_val = f[i-1][rec]
            for k in range(1, labels - 1):
                if (f[i - 1, k] < min_val):
                    min_val = f[i - 1, k]
                    rec = k
            recx[i] = rec
            add(f[i], data[i], labels)
        rec = 0
        for k in range(1, labels - 1):
            if (f[length - 1, k] < f[length - 1, rec]):
                rec = k
        for i in range(length - 1, 0):
            out[i] = rec
            next_i = recx[i]
            for j in range(rec - radius + 1, rec + radius - 1):
                if ((j >= 0) and (j < labels) and (f[i, rec] == f[i - 1, j] + smooth_term[rec, j] * weights[i - 1] + data[i, rec])):
                    next_i = j
                    break
            rec = next_i
        out[0] = rec

    def optimizeRowReverse(self, cur, init, flag):
        data = np.zeros((self.height, self.labels), dtype=np.int32)
        smooth_term = self.smooth_term
        wc = self.col_weights, wr = self.row_weights
        data_term = self.data_term
        out = np.zeros((self.height), dtype=np.int32)
        temp = np.zeros((self.labels), dtype=np.int32)
        iter_row = self.width/2
        iter_col = self.height
        num_labels = self.num_labels
        iter_odd = 1
        labels = self.labels
        radius = self.labels-1
        recx = np.zeros((iter_col), dtype=np.int32)
        f = np.zeros((iter_col, self.labels), dtype=np.int32)
        for i in range(iter_odd):
            for j in range(iter_row):
                row = 2 * j + i
                if (flag[row]):
                    for col in range(iter_col):
                        data[col] = data_term[row, col]
                        if (not init):
                            if (row > 0):
                                mul(temp, smooth_term[cur[row - 1, col]], wc[row - 1, col], labels)
                                add(data[col], temp, labels)
                            if (row < iter_row * 2 - 1):
                                mul(temp, smooth_term[cur[row + 1, col]], wc[row, col], labels)
                                add(data[col], temp, labels)
                    self.Find_Min(data, smooth_term, iter_col, out, wr[row], labels, radius, f, recx)
                    cur[row] = out

    def optimizeRow_(self, cur, init, flag):
        data = np.zeros((self.height, self.labels), dtype=np.int32)
        smooth_term = self.smooth_term
        wc = self.col_weights, wr = self.row_weights
        data_term = self.data_term
        out = np.zeros((self.height), dtype=np.int32)
        temp = np.zeros((self.labels), dtype=np.int32)
        weighted = self.weighted
        iter_row = self.width/2
        iter_col = self.height
        iter_odd = 1
        labels = self.labels
        radius = self.labels - 1
        recx = np.zeros((iter_col), dtype=np.int32)
        f = np.zeros((iter_col, self.labels), dtype=np.int32)
        for i in range(iter_odd):
            for j in range(iter_row):
                row = 2 * j + i
                if (flag[row]):
                    for col in range(iter_col):
                        data[col] = data_term[row, col]
                        if (not init):
                            if (row > 0):
                                mul(temp, smooth_term[cur[row - 1, col]], wc[row - 1, col], labels)
                                add(data[col], temp, labels)
                            if (row < iter_row * 2 - 1):
                                mul(temp, smooth_term[cur[row + 1, col]], wc[row, col], labels)
                                add(data[col], temp, labels)
                    self.Find_Min(data, smooth_term, iter_col, out, wr[row], labels, radius, f, recx)
                    cur[row] = out

    def optimizeRow(self, X, states):
        prev_state = states[0]
        state = X[0]
        tf.print(state)
        tf.print(K.softmax(prev_state))
        return X, [state]
    
    def optimizeCol(self, cur, init, flag):
        data = np.zeros((self.height, self.labels), dtype=np.int32)
        smooth_term = self.smooth_term, smooth_term_unweighted = self.smooth_term_unweighted
        wc = self.col_weights, wc_unweighted = self.col_weights_unweighted, wr = self.row_weights
        data_term = self.data_term
        out = np.zeros((self.height), dtype=np.int32)
        temp = np.zeros((self.labels), dtype=np.int32)
        iter_odd = 1
        iter_row = self.width
        iter_col = self.height/2
        radius = self.labels-1
        labels = self.labels
        recx = np.zeros((iter_row), dtype=np.int32)
        f = np.zeros((iter_row, self.labels), dtype=np.int32)
        for i in range(iter_odd):
            for j in range(iter_col):
                col = 2 * j + i
                if (flag[col]):
                    for l in range(iter_row):
                        data[l] = data_term[l, col]
                        if (not init):
                            if (col > 0):
                                mul(temp, smooth_term[cur[l, col-1]], wc[l, col-1], labels)
                                add(data[l], temp, labels)
                            if (col < iter_col*2-1):
                                mul(temp, smooth_term[cur[l, col+1]], wc[l, col], labels)
                                add(data[l], temp, labels)
                    self.Find_Min(data, smooth_term, iter_row, out, wc[col], labels, radius, labels, f, recx)
                    for l in range(iter_row):
                        cur[l, col] = out[l]

    def optimizeByCol(self, inputs):
        return inputs
    
    def optimizeByReverseCol(self, inputs):
        return inputs

    def optimizeByRow(self, inputs):
        for i in range(tf.shape(inputs)[0]):
            inputs[i] = 0
        def _step(X, states):
            return self.optimizeRow(X, states)
        def _each_row(row_input):
            tf.print(tf.shape(row_input))
            init_states = K.zeros_like(row_input[0])
            out = K.rnn(step_function=_step, inputs=tf.expand_dims(row_input, axis=0), initial_states=[init_states])
            return row_input 
        return inputs

    def optimizeByReverseRow(self, inputs):
        return inputs

    def call(self, inputs):
        if self.mode == "row":
            out = self.optimizeByRow(inputs)
        elif self.mode == "reverse_row":
            out = self.optimizeByReverseRow(inputs)
        elif self.mode == "vertical":
            out = self.optimizeByCol(inputs)
        elif self.mode == "reverse_vertical":
            out = self.optimizeByReverseCol(inputs)
        return out

    # def optimize(self, inputs):
    #     if type(inputs) is not list or len(inputs) <= 1:
    #         raise Exception('FastMRFLayer must be called on a list of tensors.' 'Got: ' + str(len(inputs)))
    #     self.data_term = inputs[0].numpy()
    #     self.cur = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.cur2 = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.curRow = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.curCol = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.rowFlag = np.ones((self.width,), dtype=np.uint8)
    #     self.colFlag = np.ones((self.height,), dtype=np.uint8)
    #     self.rowFlag_2 = np.zeros((self.width,), dtype=np.uint8)
    #     self.colFlag_2 = np.zeros((self.height,), dtype=np.uint8)
    #     # Optimize all rows independently
    #     print("optimizeRow\n")
    #     self.optimizeRow(self.curRow, True, self.rowFlag)
    #     print("optimizeCol\n")
    #     # Optimize all cols independently
    #     self.optimizeCol(self.curCol, True, self.colFlag)
    #     cdef int i, j 
    #     cdef int row_iter = self.width-1
    #     cdef int col_iter = self.height-1
    #     print(row_iter, col_iter)
    #     cdef int[:,:] cur = self.cur, curRow = self.curRow, curCol = self.curCol, cur2 = self.cur2
    #     cdef char[:] rowFlag = self.rowFlag, colFlag = self.colFlag, rowFlag_2 = self.rowFlag_2, colFlag_2 = self.colFlag_2
    #     for i in prange(row_iter, nogil=True):
    #         for j in prange(col_iter):
    #             if (rand() & 1):
    #                 cur[i, j] = curRow[i, j]
    #             else:
    #                 cur[i, j] = curCol[i, j]
    #     cdef int max_iter = self.max_iter
    #     cdef int energy = 0
    #     for i in range(max_iter):
    #         energy = self.energy(cur)
    #         print("iteration: ", i, energy)
    #         cur2[i] = cur[i]
    #         self.optimizeRow(cur, False, rowFlag)
    #         self.optimizeCol(cur, False, colFlag)
    #         rowFlag_2 = rowFlag
    #         colFlag_2 = colFlag
    #         for i in prange(row_iter+1, nogil=True):
    #             rowFlag[i] = 0
    #         for i in prange(col_iter+1, nogil=True):
    #             colFlag[i] = 0

    #         for i in prange(row_iter, nogil=True):
    #             if (rowFlag_2[i] == <int>1):
    #                 for j in prange(col_iter):
    #                     if (cur[i, j] != cur2[i, j]):
    #                         if (i > 0):
    #                             rowFlag[i-1] = 1
    #                         if (i < row_iter):
    #                             rowFlag[i+1] = 1
    #         for i in prange(col_iter, nogil=True):
    #             if (colFlag_2[i] == <int>1):
    #                 for j in prange(row_iter):
    #                     if (cur[i, j] != cur2[i, j]):
    #                         if (j > 0):
    #                             colFlag[j-1] = 1
    #                         if (i < col_iter):
    #                             colFlag[j+1] = 1
    #     return tf.Variable(self.cur)

    # def b_call(self, inputs):
    #     self.data_term = inputs
    #     # self.row_weights = inputs[1].numpy()
    #     # self.col_weights = inputs[2].numpy()
    #     self.width, self.height, self.labels = self.data_term.shape
    #     self.row_weights = np.ones((self.width, self.height), dtype=np.int32)
    #     self.row_weights_unweighted = np.ones((self.width, self.height), dtype=np.int32)
    #     self.col_weights = np.ones((self.width, self.height), dtype=np.int32)
    #     self.col_weights_unweighted = np.ones((self.width, self.height), dtype=np.int32)
    #     self.smooth_term = np.zeros((self.labels, self.labels), dtype=np.int32)
    #     self.smooth_term_unweighted = np.zeros((self.labels, self.labels), dtype=np.int32)
    #     self.smooth_term_init()
    #     self.cur = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.cur2 = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.curRow = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.curCol = np.zeros((self.width, self.height), dtype=np.int32)
    #     self.rowFlag = np.ones((self.width,), dtype=np.uint8)
    #     self.colFlag = np.ones((self.height,), dtype=np.uint8)
    #     self.rowFlag_2 = np.zeros((self.width,), dtype=np.uint8)
    #     self.colFlag_2 = np.zeros((self.height,), dtype=np.uint8)
    #     # Optimize all rows independently
    #     print("optimizeRow\n")
    #     self.optimizeRow(self.curRow, True, self.rowFlag)
    #     print("optimizeCol\n")
    #     # Optimize all cols independently
    #     self.optimizeCol(self.curCol, True, self.colFlag)
    #     row_iter = self.width-1
    #     col_iter = self.height-1
    #     print(row_iter, col_iter)
    #     cur = self.cur, curRow = self.curRow, curCol = self.curCol, cur2 = self.cur2
    #     rowFlag = self.rowFlag, colFlag = self.colFlag, rowFlag_2 = self.rowFlag_2, colFlag_2 = self.colFlag_2
    #     for i in range(row_iter):
    #         for j in range(col_iter):
    #             if (rand() & 1):
    #                 cur[i, j] = curRow[i, j]
    #             else:
    #                 cur[i, j] = curCol[i, j]
    #     max_iter = self.max_iter
    #     energy = 0
    #     for i in range(max_iter):
    #         energy = self.energy(cur)
    #         print("iteration: ", i, energy)
    #         cur2[i] = cur[i]
    #         self.optimizeRow(cur, False, rowFlag)
    #         self.optimizeCol(cur, False, colFlag)
    #         rowFlag_2 = rowFlag
    #         colFlag_2 = colFlag
    #         for i in prange(row_iter+1, nogil=True):
    #             rowFlag[i] = 0
    #         for i in prange(col_iter+1, nogil=True):
    #             colFlag[i] = 0

    #         for i in prange(row_iter, nogil=True):
    #             if (rowFlag_2[i] == <int>1):
    #                 for j in prange(col_iter):
    #                     if (cur[i, j] != cur2[i, j]):
    #                         if (i > 0):
    #                             rowFlag[i-1] = 1
    #                         if (i < row_iter):
    #                             rowFlag[i+1] = 1
    #         for i in prange(col_iter, nogil=True):
    #             if (colFlag_2[i] == <int>1):
    #                 for j in prange(row_iter):
    #                     if (cur[i, j] != cur2[i, j]):
    #                         if (j > 0):
    #                             colFlag[j-1] = 1
    #                         if (i < col_iter):
    #                             colFlag[j+1] = 1
    #     return tf.Variable(self.cur)
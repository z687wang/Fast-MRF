import numpy as np

class Config(object):
    NAME = None
    GPU_COUNT = 1
    IMAGES_PER_GPU = 4
    BATCH_SIZE = 32
    STEPS_PER_EPOCH = 500
    MAX_ITERATION = 20000
    VALIDATION_STEPS = 50
    BACKBONE = "vgg16"
    NUM_CLASSES = 1
    IMAGE_RESIZE_MODE = "square"
    IMAGE_MIN_DIM = 800
    IMAGE_MAX_DIM = 256
    IMAGE_MIN_SCALE = 0
    IMAGE_CHANNEL_COUNT = 3
    MEAN_PIXEL = np.array([123.7, 116.8, 103.9])
    learning_rate = 0.0001
    WEIGHT_DECAY = 0.00005
    debug = False
    
    def __init__(self):
        self.IMAGE_SHAPE = np.array([self.IMAGE_MAX_DIM, self.IMAGE_MAX_DIM, self.IMAGE_CHANNEL_COUNT])
        self.IMAGE_META_SIZE = 1 + 3 + 3 + 4 + 1 + self.NUM_CLASSES
    
    def display(self):
        """Display Configuration values."""
        print("\nConfigurations:")
        for a in dir(self):
            if not a.startswith("__") and not callable(getattr(self, a)):
                print("{:30} {}".format(a, getattr(self, a)))
        print("\n")
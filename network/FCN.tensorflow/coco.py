from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.callbacks import TensorBoard
from model import vgg19
import os
import numpy as np
import time
from config import Config
import utils
import TensorflowUtils as tf_utils
import imgaug
import model
import datetime
import time
from six.moves import xrange


class CoCoConfig(Config):
    NAME = "coco"
    IMAGES_PER_GPU = 2
    NUM_CLASSES = 2

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN on MS COCO.')
    parser.add_argument("--mode",
                        default="train",
                        metavar="<mode>",
                        help="'train' or 'test' on COCO")
    parser.add_argument('--dataset', required=False,
                        default='./coco/',
                        metavar="/path/to/coco/",
                        help='Directory of the COCO dataset')
    parser.add_argument('--logs_dir', required=False,
                        default='logs/',
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    
    parser.add_argument('--model_dir', required=False,
                        default='Model_zoo/',
                        metavar="/path/to/models/",
                        help='models directory (default=Model_zoo/)')

    parser.add_argument('--limit', required=False,
                        default=500,
                        metavar="<image count>",
                        help='Images to use for evaluation (default=500)')

    
    args = parser.parse_args()
    print("Mode: ", args.mode)
    print("Dataset: ", args.dataset)
    print("Logs: ", args.logs_dir)

    MODEL_URL = 'http://www.vlfeat.org/matconvnet/models/beta16/imagenet-vgg-verydeep-19.mat'

    # Configurations
    if args.mode == "train":
        config = CoCoConfig()
    else:
        class InferenceConfig(CoCoConfig):
            # Set batch size to 1 since we'll be running inference on
            # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1
        config = InferenceConfig()
    config.display()

class CoCoDataset(utils.Dataset):
    def load_coco(self, dataset_dir, subset, class_ids=None, return_coco=False, year=2017):
        self.class_ids = class_ids
        self.num_classes = len(self.class_ids)
        coco = COCO('{}/annotations/instances_{}{}.json'.format(dataset_dir, subset, year))
        image_dir = "{}/{}{}".format(dataset_dir, subset, year)
        if not class_ids:
            class_ids = sorted(coco.getCatIds())
        if class_ids:
            image_ids = []
            for id in class_ids:
                image_ids.extend(list(coco.getImgIds(catIds=[id])))
            image_ids = list(set(image_ids))
        else:
            image_ids = list(coco.imgs.keys())
        for i in image_ids:
            self.add_image("coco", 
                image_id=i, 
                path=os.path.join(image_dir, coco.imgs[i]['file_name']),
                width=coco.imgs[i]["width"],
                height=coco.imgs[i]["height"],
                annotations=coco.loadAnns(coco.getAnnIds(imgIds=[i], catIds=class_ids, iscrowd=None)))
        if return_coco:
            return coco
    
    def load_mask(self, image_id):
        image_info = self.image_info[image_id]
        class_masks = []
        class_mask_map = {}
        annotations = self.image_info[image_id]["annotations"]
        class_count = 0
        count = 0
        
        for class_id in self.class_ids:
            m = np.zeros([image_info["height"], image_info["width"]], dtype=bool)
            class_masks.append(m)
            class_mask_map[class_id] = class_count
            class_count += 1
        
        for annotation in annotations:
            class_id  = annotation['category_id']
            if class_id in self.class_ids:
                count += 1
                m = self.annToMask(annotation, image_info["height"], image_info["width"])
                o_mask = class_masks[class_mask_map[class_id]]
                class_masks[class_mask_map[class_id]] = np.logical_or(o_mask, m)
                if m.max() < 1:
                    continue

        if self.class_ids:
            mask = np.stack(class_masks, axis=2).astype(np.bool)
            self.class_ids = np.array(self.class_ids, dtype=np.int32)
            return mask, self.class_ids
        else:
            return super(CoCoDataset, self).load_mask(image_id)
    
    def annoToRLE(self, ann, height, width):
        segm = ann['segmentation']
        if isinstance(segm, list):
            rles = maskUtils.frPyObjects(segm, height, width)
            rle = maskUtils.merge(rles)
        elif isinstance(segm['counts'], list):
            rle = maskUtils.frPyObjects(segm, height, width)
        else:
            rle = ann['segmentation']
        return rle
    
    def annToMask(self, ann, height, width):
        rle = self.annoToRLE(ann, height, width)
        m = maskUtils.decode(rle)
        return m

def build_coco_results(dataset, image_ids, class_ids, scores, masks):
    results = []
    for image_id in image_ids:
        for i in range(scores.shape[0]):
            class_id = class_ids[i]
            score = scores[i]
            mask = masks[:, :, i]
            result = {
                "image_id": image_id,
                "category_id": dataset.get_source_class_id(class_id, "coco"),
                "score": score,
                "segmentation": maskUtils.encode(np.asfortranarray(mask))
            }
            results.append(result)
    return results

def evaluate_coco(model, dataset, coco, limit=0, image_ids=None):
    if limit:
        image_ids = image_ids[:limit]
    coco_image_ids = [dataset.image_info[id]["id"] for id in image_ids]
    t_prediction = 0
    t_start = time.time()
    results = []
    for i, image_id in enumerate(image_ids):
        image = dataset.load_image(image_id)
        t = time.time()
        r = model.detect([image], verbose=0)[0]
        t_prediction += (time.time() - t)
        image_results = build_coco_results(dataset, coco_image_ids[i:i+1], r["class_ids"], r["scores"], r["masks"].astype(np.uint8))
        results.extend(image_results)
    
    coco_results = coco.loadRes(results)

    cocoEval = COCOeval(coco, coco_results, "segm")
    cocoEval.params.imgIds = coco_image_ids
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()

    print("Prediction time: {}. Average {}/image".format(
        t_prediction, t_prediction / len(image_ids)))
    print("Total time: ", time.time() - t_start)



def iou_metrics(gt, pred, batch_size):
    iou_score = 0
    for i in range(batch_size):
        intersection = np.logical_and(gt[i], pred[i])
        union = np.logical_or(gt[i], pred[i])
        iou_score += np.sum(intersection) / np.sum(union)
    iou_score /= batch_size
    return iou_score

def mean_iou(y_true, y_pred, smooth=1):
    """
    IoU = (|X and Y|)/ (|X or Y|)
    """
    return 1
    # y_pred = tf.argmax(y_pred, axis=3, name="prediction")
    # y_pred = tf.expand_dims(y_pred, axis=3)
    # y_pred = np.array(y_pred)
    # iou_m = iou_metrics(y_true, y_pred, config.BATCH_SIZE)
    # return iou_m
  
def sparse_loss(annotation, logits):
    loss = tf.reduce_mean((tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,
                                                                          labels=tf.cast(tf.squeeze(annotation, axis=[3]), tf.int32),
                                                                          name="entropy")))
    return loss

def write_summary(board, names, logs, batch_num):
    for name, value in zip(names, logs):
        summary = tf.summary()
        summary_value = summary.value.add()
        summary_value.simple_value = value
        summary_value.tag = name
        board.writer.add_summary(summary, batch_num)
        board.writer.flush()

vgg_model = vgg19()
adam = keras.optimizers.Adam(lr=config.learning_rate)
vgg_model.compile(optimizer=adam, loss=[sparse_loss], metrics=[mean_iou])
vgg_model.summary()
dataset_train = CoCoDataset()
dataset_train.load_coco(args.dataset, "train", [1])
dataset_train.prepare()
# Validation dataset
dataset_val = CoCoDataset()
val_type = "val"
dataset_val.load_coco(args.dataset, "val", [1])
dataset_val.prepare()

train_gen = model.data_generator(dataset_train, config)
val_gen = model.data_generator(dataset_val, config)

# Train or evaluate
if args.mode == "train":
    
    board = TensorBoard(args.logs_dir)
    board.set_model(vgg_model)
    # vgg_model.load_weights(args.logs_dir + 'model-5400.h5')
    
    MAX_ITERATION = config.MAX_ITERATION
    batch_size = config.BATCH_SIZE

    train_names = ['train_loss', 'train_iou']
    val_names = ['val_loss', 'val_iou']
    train_log_dir = args.logs_dir + 'train/'
    test_log_dir = args.logs_dir + 'test/'
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    test_summary_writer = tf.summary.create_file_writer(test_log_dir)

    for itr in xrange(MAX_ITERATION):
        train_images, train_classes_id, train_annotations = next(train_gen)
        train_loss = vgg_model.train_on_batch(train_images, train_annotations)
        train_loss = train_loss[0]

        with train_summary_writer.as_default():
            tf.summary.scalar('loss', train_loss, step=itr)

        if (itr) % 5 == 0: 
            print("Step: %d, Train_loss:%g" % (itr, train_loss))
        if (itr % 50) == 0:
            pred = vgg_model.predict_on_batch(train_images)
            y_pred = tf.argmax(pred, axis=3, name="prediction")
            y_pred = tf.expand_dims(y_pred, axis=3)
            y_pred = np.array(y_pred)
            iou_score = iou_metrics(train_annotations, y_pred, config.BATCH_SIZE)
            with train_summary_writer.as_default():
                tf.summary.scalar('loss', train_loss, step=itr)
                tf.summary.scalar('iou', iou_score, step=itr)
            # write_summary(board, train_names, [train_loss, iou_score], itr)
            print("Step: %d, IoU Score:%g" % (itr, iou_score))
        if (itr) % 200 == 0:
            val_images, val_classes_id, val_annotations = next(train_gen)
            valid_loss = vgg_model.test_on_batch(val_images, train_annotations)[0]
            print("%s ---> Validation_loss: %g" % (datetime.datetime.now(), valid_loss))
            pred = vgg_model.predict_on_batch(val_images)
            y_pred = tf.argmax(pred, axis=3, name="prediction")
            y_pred = tf.expand_dims(y_pred, axis=3)
            y_pred = np.array(y_pred)
            iou_score = iou_metrics(val_annotations, y_pred, config.BATCH_SIZE)
            print("Step: %d, IoU Score:%g" % (itr, iou_score))
            with test_summary_writer.as_default():
                tf.summary.scalar('loss', valid_loss, step=itr)
                tf.summary.scalar('iou', iou_score, step=itr)
            if (itr) % 300 == 0:
                vgg_model.save_weights(args.logs_dir + 'model-' + str(itr) + '.h5')
                print("Model Saved")

        # # vgg_model.train(train_gen, val_gen)
        # vgg_model.evaluate(val_gen)
        

elif args.mode == "test":
    vgg_model.load_weights(args.logs_dir + 'model-5400.h5')
    for i in range(3):
        train_images, train_classes_id, train_annotations = next(train_gen)
        pred = vgg_model.predict_on_batch(train_images)
        y_pred = tf.argmax(pred, axis=3, name="prediction")
        y_pred = tf.expand_dims(y_pred, axis=3)
        y_pred = np.array(y_pred)
        iou_score = iou_metrics(train_annotations, y_pred, config.BATCH_SIZE)
        print("Iteration: %d, IoU Score: %g" % (i, iou_score))
        y_pred = np.squeeze(y_pred, axis=3)
        train_annotations = np.squeeze(train_annotations, axis=3)
        for itr in range(config.BATCH_SIZE):
            current_file_name = str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H_%M_%S'))
            tf_utils.save_image(train_images[itr].astype(np.uint8), args.logs_dir, name="inp_" + current_file_name)
            tf_utils.save_image((train_annotations[itr]*255).astype(np.uint8), args.logs_dir, name="gt_" + current_file_name)
            tf_utils.save_image((y_pred[itr]*255).astype(np.uint8), args.logs_dir, name="pred_" + current_file_name)
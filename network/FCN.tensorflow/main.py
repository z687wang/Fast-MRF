from FastMRFLayer import FastMRFLayer
import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
import tensorflow.keras.backend as K

inputs = tf.keras.layers.Input(shape=(16, 16, 3), dtype=tf.float64)
# input_right = tf.keras.layers.Input(shape=(16,))

# left_branch = tf.keras.layers.Dense(32, input_shape=(16,))(input_left)
# right_branch = tf.keras.layers.Dense(32, input_shape=(16,))(input_right)
x = tf.keras.layers.Conv2D(6, kernel_size=(2, 2), strides=(1, 1), padding='SAME')(inputs)
x = tf.cast(x, tf.float64)
x = tf.keras.layers.Conv2DTranspose(2, kernel_size=(2, 2), strides=(1, 1), padding='SAME')(x)
layer = FastMRFLayer("row", 10, 1)
output = layer(x)
model = tf.keras.models.Model(inputs=inputs, outputs=output)
model.run_eagerly = True
model.compile(optimizer='sgd',
              loss='mean_squared_error',
              metrics=['accuracy'])
model.summary()

optimizer = tf.keras.optimizers.Adam()

def train_step(x, y):
    with tf.GradientTape() as tape:
        logits = model(x, training=True)     
        loss_value = tf.keras.losses.MSE(y_true=y, y_pred=logits)
        grads = tape.gradient(loss_value, model.trainable_variables)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))
        return loss_value

for i in range(100):
    loss = train_step(np.ones((3, 16, 16, 3)), np.zeros((3, 16, 16, 2)))
    print("iteration:", i, "loss:", tf.math.reduce_mean(loss[0]))

#include<iostream>
#include<omp.h>
#include<cmath>
#include<smmintrin.h>
#include<cstdio>
#include<cstring>
#define fr(i, a, b) for(int i = a; i <= b; ++i)
#define TYPESSE __m128i
#define LIMIT (1 << 30)
#define MAXITER 100
#define LL int
int RADIUS = 4;
typedef int (*comp)(int,int,int,int);
using namespace std;

int **create2d(int m,int n) {
	int **tmp = new int* [m];
	#pragma omp parallel for
	fr(i, 0, m-1)
		tmp[i] = new int[n];
	return tmp;
}
int ***create3d(int m, int n, int o) {
	int ***tmp = new int**[m];
	fr(i, 0, m-1)
		tmp[i] = create2d(n,o);
	return tmp;
}

struct mrfGrid{
	int m, n, l, l4, L, **wr, **wc, **wc2, **s, **s2, ***d, **cur, **cur2, type, radius, tw;
	bool weighted;
	comp decision;
	LL val;
	inline void add(int *a, int *b){
		TYPESSE *ma = (TYPESSE*)a, *mb = (TYPESSE*)b;
		fr(iter, 0, l4)
			ma[iter] += mb[iter];
	}
	inline void add(int *a, int r){
		TYPESSE *ma = (TYPESSE*)a, tmp = _mm_set1_epi32(r);
		fr(iter, 0, l4)
			ma[iter] += tmp;
	}
	inline void makeMin(int *a, int *b){
		TYPESSE *ma = (TYPESSE*)a, *mb = (TYPESSE*)b;
		fr(iter, 0, l4)
			ma[iter] = _mm_min_epi32(ma[iter], mb[iter]);
	}
	inline void mul(int* c, int *a, int b){
		TYPESSE *ma = (TYPESSE*)a, tmp = _mm_set1_epi32(b), *mc = (TYPESSE*)c;
		fr(iter, 0, l4)
			mc[iter] = _mm_mullo_epi32(ma[iter], tmp);
	}
	inline void makeMin(int *a, int mi){
		TYPESSE *ma = (TYPESSE*)a, tmp = _mm_set1_epi32(mi);
		fr(iter, 0, l4)
			ma[iter] = _mm_min_epi32(ma[iter], tmp);
	}

	mrfGrid(int m_ = 0, int n_ = 0, int l_ = 0, comp decision_ = NULL): m(m_), n(n_), l(l_), decision(decision_) {
		L = (l+3)/4*4;
		l4 = (L>>2)-1;
        cout << "l4: " << l4 << endl;
		wr = create2d(m, n);
		wc = create2d(m, n);
		wc2 = create2d(n, m);
		s = create2d(L, L);
		d = create3d(m, n, L);
		#pragma omp parallel for
		fr(i, 0, m-1)
			fr(j, 0, n-1)
				wr[i][j] = wc[i][j] = 1;
	}


	inline LL energy(int **r){
		LL sum = 0;
		if(weighted)
			#pragma omp parallel for reduction (+:sum)
			fr(i, 0, m-1)
				fr(j, 0, n-1)
					sum += d[i][j][r[i][j]] + (i == m-1? 0: wc[i][j] * s[r[i][j]][r[i+1][j]]) + (j == n-1? 0: wr[i][j] * s[r[i][j]][r[i][j+1]]);
		else
			#pragma omp parallel for reduction (+:sum)
			fr(i, 0, m-1)
				fr(j, 0, n-1)
					sum += d[i][j][r[i][j]] + (i == m-1? 0: s2[r[i][j]][r[i+1][j]]) + (j == n-1? 0: s2[r[i][j]][r[i][j+1]]);
		return sum;
	}

	inline void calBF(int *data[], int len, int *out, int *w){
		int f[len][L], min;
		memcpy(f[0], data[0], l * sizeof(int));
		int rec, recx[len];
		int left[L], right[L];
		fr(i, 1, len-1) {
			memcpy(f+i, f+i-1, l*sizeof(int));
			fr(j, 1, radius-1){
				memcpy(left, f[i-1]+j, (l-j) * sizeof(int));
				memcpy(right+j, f[i-1], (l-j) * sizeof(int));
				memset(left+l-j, 63, j * sizeof(int));
				memset(right, 63, j * sizeof(int));
				makeMin(left, right);
				add(left, s[0][j] * w[i-1]);
				makeMin(f[i], left);
			}
			min = f[i-1][rec=0];
			fr(k, 1, l-1)
				if(f[i-1][k] < min)
					min = f[i-1][rec=k];
			recx[i] = rec;
			makeMin(f[i], min + tw * w[i-1]);
            // + min(C(x-1, y, k) + p(-k))
			add(f[i], data[i]);
		}
		rec = 0;
    	fr(k, 1, l-1)
			if(f[len-1][k] < f[len-1][rec])
				rec = k;
		for(int i = len - 1; i > 0; --i){
			out[i] = rec;
			int next = recx[i];
			fr(j, rec-radius+1, rec+radius-1)
				if(j >= 0 && j < l && f[i][rec] == f[i-1][j] + s[rec][j] * w[i-1] + data[i][rec]){
					next = j;
					break;
				}
			rec = next;
		}
		out[0] = rec;	
	}

	inline void calL2(int *data[],int len,int *out,int *w) {
		int f[len][L], mi;
		memcpy(f[0], data[0], l*sizeof(int));
		int rec, last, tmp, ind;
		int pre[len][l], lst[l+1], split[l+1];
		fr(i, 1, len-1){
			lst[last=1] = 0;
			split[0] = -1;
			fr(j, 1, l-1){
				while(split[last-1] >= (tmp = decision(lst[last], j, f[i-1][j]-f[i-1][lst[last]], w[i-1])))
					if(--last==0)
						break;
				if(last==0)
					lst[++last]=j;
				else
					if(tmp<l-1){
						split[last]=tmp;
						lst[++last]=j;
					}
			}
			split[last] = l-1;
			ind = 1;
			fr(j, 0, l-1){
				while(j > split[ind])
					++ind;
				pre[i][j] = tmp = lst[ind];
				f[i][j] = f[i-1][tmp] + s[tmp][j] * w[i-1];
			}	
			if(radius < l-1){
				rec = 0;
				fr(k, 0, l-1)
					if(f[i-1][k] < f[i-1][rec])
						rec = k;
				mi = f[i-1][rec] + tw*w[i-1];
				fr(k, 0, l-1)
					if(f[i][k] > mi){
						f[i][k] = mi;
						pre[i][k] = rec;
					}
			}
			add(f[i], data[i]);
		}
		rec = 0;
		fr(k, 1, l-1)
			if(f[len-1][k] < f[len-1][rec])
				rec = k;
		for(int i = len-1; i >= 0; --i){
			out[i] = rec;
			rec = pre[i][rec];
		}
	}

	inline void calL1(int *data[], int len, int *out, int *w){
		int f[len][L],mi;
		int rec;
		memcpy(f[0], data[0], l*sizeof(int));
		int pre[len][l];
		fr(i, 1, len-1){
			mi = f[i-1][0];
			rec = 0;
			int tmp = 0, inc = w[i-1];
			fr(k, 0, l-1){
				if(mi > f[i-1][k] - tmp){
					mi = f[i-1][k] - tmp;
					rec = k;
				}
				f[i][k] = mi + tmp;
				pre[i][k] = rec;
				tmp += inc;
			}
			tmp -= inc;
			mi = f[i-1][l-1] + tmp;
			rec = l-1;
			for(int k = l-1; k >= 0; --k){
				if(mi > f[i-1][k] + tmp){
					mi = f[i-1][k] + tmp;
					rec = k;
				}
				if(f[i][k] > mi-tmp){
					f[i][k] = mi-tmp;
					pre[i][k] = rec;
				}
				tmp -= inc;
			}
			if(radius < l-1){
				mi = f[i-1][rec=0];
				fr(k, 1, l-1)
					if(f[i-1][k] < mi)
						mi = f[i-1][rec=k];
				mi += tw * w[i-1];
				fr(k, 0, l-1)
					if(f[i][k] > mi){
						f[i][k] = mi;
						pre[i][k] = rec;
					}
			}
			add(f[i], data[i]);
		}
		rec = 0;
		fr(k, 1, l-1)
			if(f[len-1][k] < f[len-1][rec])
				rec = k;
		for(int i = len - 1; i > 0; --i){
			out[i] = rec;
			rec = pre[i][rec];
		}
		out[0] = rec;
	}

	inline void updateRow(int **cur, bool init, bool *flag){
		// optimize even rows and odd rows separately
		fr(st, 0, 1)
		#pragma omp parallel
		{
			int id2 = omp_get_thread_num() << 1, nthrds2 = omp_get_num_threads() << 1;
			int data[n][L], *data2[n], tmp[L];
			int out[n];
			fr(i, 0, n-1)
				data2[i] = data[i];
			
			for(int row = st + id2; row < m; row += nthrds2)
				if(flag[row]){
					if(weighted)
						fr(j, 0, n-1){
							memcpy(data[j], d[row][j], l * sizeof(int));
							if(!init){
								if(row > 0){
									// (x, y-1)
									mul(tmp, s[cur[row-1][j]], wc[row-1][j]);
									add(data[j], tmp);
								}
								if(row < m-1){
									// (x, y+1)
									mul(tmp, s[cur[row+1][j]], wc[row][j]);
									add(data[j], tmp);
								}
							}
						}
					else					
						fr(j, 0, n-1){
							memcpy(data[j], d[row][j], l*sizeof(int));
							if(!init){
								if(row > 0)
									add(data[j], s2[cur[row-1][j]]);
								if(row < m-1)
									add(data[j], s2[cur[row+1][j]]);
							}
						}
                    cout << "update row: radius: " << radius << ", RADIUS: " << RADIUS << endl;
					if(radius <= RADIUS)
						calBF(data2, n, out, wr[row]);
					else
						if(type == 1)
							calL1(data2, n, out, wr[row]);
						else
							calL2(data2, n, out, wr[row]);
					memcpy(cur[row], out, n * sizeof(int));
				}
		}
	}

	inline void updateCol(int **cur,bool init,bool *flag){
		fr(st,0,1)
		#pragma omp parallel
		{
			int id2=omp_get_thread_num()<<1,nthrds2=omp_get_num_threads()<<1;
			int data[m][L],*data2[m],tmp[L];
			int out[m];
			fr(j,0,m-1)
				data2[j]=data[j];
			for(int col = st + id2; col < n; col += nthrds2)
				if(flag[col]){
					if(weighted)
						fr(i,0,m-1){
							memcpy(data[i], d[i][col], l * sizeof(int));
							if(!init){
								if(col > 0){
									mul(tmp, s[cur[i][col-1]], wr[i][col-1]);
									add(data[i], tmp);
								}
								if(col < n-1){
									mul(tmp, s[cur[i][col+1]], wr[i][col]);
									add(data[i], tmp);
								}
							}
						}
					else
						fr(i, 0, m-1){
							memcpy(data[i], d[i][col], l * sizeof(int));
							if(!init){
								if(col > 0)
									add(data[i], s2[cur[i][col-1]]);
								if(col < n-1)
									add(data[i], s2[cur[i][col+1]]);
							}
						}
					cout << "update row: radius: " << radius << ", RADIUS: " << RADIUS << endl;
					if(radius <= RADIUS)
						calBF(data2, m, out, wc[col]);
					else
						if(type==1)
							calL1(data2, m, out, wc2[col]);
						else
							calL2(data2, m, out, wc2[col]);
					fr(i, 0, m-1)
						cur[i][col] = out[i];
				}
		}
	}

	int **BCD() {
		radius = l - 1;
		while(radius > 0 && s[0][l-1] == s[0][radius-1])
			radius--;
		type = 1;
		
		fr(i, 0, radius - 1)
			if(s[0][i] != i)
				type = 2;
		
		tw = s[0][l-1];
		weighted = false;
		int lambda = wr[0][0];
		
		#pragma omp parallel for
		fr(i, 0, m-1) {
			fr(j, 0, n-1) {
				wc2[j][i] = wc[i][j];
				if(wr[i][j] != lambda && j < n-1 || wc[i][j] != lambda && i < m-1)
					weighted = true;
			}
		}

		if(!weighted){
			s2 = create2d(L, L);
			#pragma omp parallel for
			fr(i, 0, l-1)
				fr(j, 0, l-1)
					s2[i][j] = s[i][j] * lambda;
		}

		cur = create2d(m,n);
		cur2 = create2d(m,n);
		int **curRow = create2d(m, n);
		int **curCol = create2d(m, n);
		bool rowFlag[m], colFlag[n], rowFlag2[m], colFlag2[n];
		
		memset(rowFlag, true, sizeof(rowFlag));
		memset(colFlag, true, sizeof(colFlag));
		// optimizing all rows independently
		updateRow(curRow, true, rowFlag);
		// optimizing all columns independently
		updateCol(curCol, true, colFlag);
		
		// Initialize: the inital label of each of pixel is chosen randomly from its labels from optimizing all rows independenly 
		// and all columns independently
		#pragma omp parallel for
		fr(i, 0, m-1)
			fr(j, 0, n-1)
				if(rand() & 1)
					cur[i][j] = curRow[i][j];
				else
					cur[i][j] = curCol[i][j];
		
		LL pre;
		fr(ti, 1, MAXITER){
			val = energy(cur);
			cout << "Iteration " << ti << ": " << val << endl;
			
			if(val >= pre && ti > 1)
				break;
			pre = val;
			
			#pragma omp parallel for
			fr(i, 0, m-1)
				memcpy(cur2[i], cur[i], n * sizeof(int));
			
			updateRow(cur, false, rowFlag);
			updateCol(cur, false, colFlag);
			memcpy(rowFlag2, rowFlag, sizeof(rowFlag));
			memcpy(colFlag2, colFlag, sizeof(colFlag));
			memset(rowFlag, false, sizeof(rowFlag));
			memset(colFlag, false, sizeof(colFlag));
			
			#pragma omp parallel for
			fr(i, 0, m-1)
				if(rowFlag2[i])
					fr(j, 0, n-1)
						if(cur[i][j] != cur2[i][j]){
							if(i>0)
								rowFlag[i-1] = true;
							if(i<m-1)
								rowFlag[i+1] = true;
						}
			
			#pragma omp parallel for
			fr(j,0,n-1)
				if(colFlag2[j])
					fr(i,0,m-1)
						if(cur[i][j] != cur2[i][j]){
							if(j>0)
								colFlag[j-1]=true;
							if(j<n-1)
								colFlag[j+1]=true;
						}
		}
		return cur;
	}
};

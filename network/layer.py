import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

class MRF_Layer(nn.Module):
    def __init__(self, width, height, channel, mode="row"):
        super(MRF_Layer, self).__init__()
        self.mode = mode
        self.conv1 = nn.Conv2d(2, 6, 3)
        self.conv2 = nn.Conv2d(2, 1, 3, 1, 0)
        self.conv3 = nn.Conv2d(2, 1, 3, 1, 0)
        self.softmax = torch.nn.Softmax(dim=1)
        self.labels = channel
        self.width = width
        self.height = height
        self.smooth_term_init()

    def smooth_term_init(self):
        l = self.labels
        self.smooth_term = torch.ones(l, l)
        for i in range(l):
            for j in range(l):
                if (i > j):
                    diff = i - j
                else:
                    diff = j - i
                self.smooth_term[i, j] = diff
                self.smooth_term[j, i] = diff

    def optimize_row(self, x):
        prev = x[:, :, 0, :].clone()
        for i in range(1, x.shape[2], 1): # width
            for l1 in range(x.shape[1]):
                prev_clone = prev.clone()
                for l2 in range(prev.shape[1]):
                    prev_clone[:, l2] =  prev_clone[:, l2] + self.smooth_term[l1, l2] * self.row_weights[:, i-1, :]
                x[:, l1, i, :] = x[:, l1, i, :] + torch.max(prev_clone, dim=1)[0]
            prev = x[:, :, i, :].clone()
        return x
    
    def optimize_row_reverse(self, x):
        prev = x[:, :, x.shape[2]-1, :].clone()
        for i in range(x.shape[2]-2, -1, -1): # width
            for l1 in range(x.shape[1]):
                prev_clone = prev.clone()
                for l2 in range(prev.shape[1]):
                    prev_clone[:, l2] =  prev_clone[:, l2] + self.smooth_term[l1, l2] * self.row_weights[:, i, :]
                x[:, l1, i, :] = x[:, l1, i, :] + torch.max(prev_clone, dim=1)[0]
            prev = x[:, :, i, :].clone()
        return x

    def optimize_col(self, x):
        prev = x[:, :, :, 0].clone()
        for j in range(1, x.shape[3], 1): # height
            for l1 in range(x.shape[1]):
                prev_clone = prev.clone()
                for l2 in range(prev.shape[1]):
                    prev_clone[:, l2] =  prev_clone[:, l2] + self.smooth_term[l1, l2] * self.col_weights[:, :, j-1]
                x[:, l1, :, j] = x[:, l1, :, j] + torch.max(prev_clone, dim=1)[0]
            prev = x[:, :, :, j].clone()
        return x
    
    def optimize_col_reverse(self, x):
        prev = x[:, :, :, x.shape[3]-1].clone()
        for j in range(x.shape[3]-2, -1, -1):
            for l1 in range(x.shape[1]):
                prev_clone = prev.clone()
                for l2 in range(prev.shape[1]):
                    prev_clone[:, l2] =  prev_clone[:, l2] + self.smooth_term[l1, l2] * self.col_weights[:, :, j]
                x[:, l1, :, j] = x[:, l1, :, j] + torch.max(prev_clone, dim=1)[0]
            prev = x[:, :, :, j].clone()
        return x
    
    def forward(self, x, row_weights, col_weights):
        self.row_weights = row_weights
        self.col_weights = col_weights
        if self.mode == "row":
            out = self.optimize_row(x)
        elif self.mode == "reverse_row":
            out = self.optimize_row_reverse(x)
        elif self.mode == "col":
            out = self.optimize_col(x)
        elif self.mode == "reverse_col":
            out = self.optimize_col_reverse(x)
        return out
import argparse
import datetime
import os
import os.path as osp

import torch
import yaml
from fcn8s import FCN8s
from fcn16s import FCN16s
from fcn32s import FCN32s
from trainer import Trainer
from coco import CoCoDataset
from train_fcn32s import get_parameters
from train_fcn32s import git_hash
from utils import remove_state


here = osp.dirname(osp.abspath(__file__))


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument('-g', '--gpu', type=int, required=True, help='gpu id')
    parser.add_argument('--resume', help='checkpoint path')
    # configurations (same configuration as original work)
    # https://github.com/shelhamer/fcn.berkeleyvision.org
    parser.add_argument(
        '--max-iteration', type=int, default=100000, help='max iteration'
    )
    parser.add_argument('--dataset', required=False,
                        default='./FCN.tensorflow/coco/',
                        metavar="/path/to/coco/",
                        help='Directory of the COCO dataset')
    parser.add_argument(
        '--lr', type=float, default=0.001, help='learning rate',
    )
    parser.add_argument(
        '--weight-decay', type=float, default=0.0005, help='weight decay',
    )
    parser.add_argument(
        '--momentum', type=float, default=0.99, help='momentum',
    )
    parser.add_argument(
        '--pretrained-model',
        default=FCN32s.download(),
        help='pretrained model of FCN32s',
    )
    args = parser.parse_args()

    args.model = 'FCN16s'
    args.git_hash = git_hash()

    now = datetime.datetime.now()
    args.out = osp.join(here, 'logs', now.strftime('%Y%m%d_%H%M%S.%f'))

    os.makedirs(args.out)
    with open(osp.join(args.out, 'config.yaml'), 'w') as f:
        yaml.safe_dump(args.__dict__, f, default_flow_style=False)

    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)
    cuda = torch.cuda.is_available()

    torch.manual_seed(1337)
    if cuda:
        torch.cuda.manual_seed(1337)

    # 1. dataset

    root = osp.expanduser('~/data/datasets')
    kwargs = {'num_workers': 4, 'pin_memory': True} if cuda else {}
    train_loader = torch.utils.data.DataLoader(
        CoCoDataset(args.dataset, "train", [1]),
        batch_size=64, shuffle=True, **kwargs)
    val_loader = torch.utils.data.DataLoader(
        CoCoDataset(args.dataset, "val", [1]),
        batch_size=64, shuffle=False, **kwargs)

    # 2. model

    model = FCN16s(n_class=2)
    start_epoch = 0
    start_iteration = 0
    if args.resume:
        checkpoint = torch.load(args.resume)
        model.load_state_dict(checkpoint['model_state_dict'])
        start_epoch = checkpoint['epoch']
        start_iteration = checkpoint['iteration']
    else:
        fcn32s = FCN32s(n_class=2)
        state_dict = torch.load(args.pretrained_model)
        new_state_dict = remove_state(state_dict)
        state = fcn32s.state_dict()
        state.update(new_state_dict)
        fcn32s.load_state_dict(state)
        model.copy_params_from_fcn32s(fcn32s)
        count = 0
        for children in model.children():
            if count < 36:
                for params in children.parameters():
                    params.requires_grad = False
                count += 1
    if cuda:
        model = model.cuda()

    # 3. optimizer

    optim = torch.optim.SGD(
        [
            {'params': get_parameters(model, bias=False)},
            {'params': get_parameters(model, bias=True),
             'lr': args.lr, 'weight_decay': args.weight_decay},
        ],
        lr=args.lr,
        momentum=args.momentum,
        weight_decay=args.weight_decay)
    if args.resume:
        optim.load_state_dict(checkpoint['optim_state_dict'])

    trainer = Trainer(
        cuda=cuda,
        model=model,
        optimizer=optim,
        init_lr = args.lr,
        train_loader=train_loader,
        val_loader=val_loader,
        out=args.out,
        max_iter=args.max_iteration,
        interval_validate=200,
    )
    trainer.epoch = start_epoch
    trainer.iteration = start_iteration
    trainer.train()


if __name__ == '__main__':
    main()
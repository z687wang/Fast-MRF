from __future__ import division, print_function, absolute_import
from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize
import os
import numpy


build_type="optimized"
#build_type="debug"

# Modules involving numerical computations
#
extra_compile_args_math_optimized    = ['-std=c++11', '-march=native', '-O3', '-m64', '-msse4', '-fopenmp']
extra_compile_args_math_debug        = ['-march=native', '-O0', '-g']
extra_link_args_math_optimized       = ['-fopenmp']
extra_link_args_math_debug           = []

# Modules that do not involve numerical computations
#
extra_compile_args_nonmath_optimized = ['-O3']
extra_compile_args_nonmath_debug     = ['-O0', '-g']
extra_link_args_nonmath_optimized    = []
extra_link_args_nonmath_debug        = []

my_include_dirs = ["."]

if build_type == 'optimized':
    my_extra_compile_args_math    = extra_compile_args_math_optimized
    my_extra_compile_args_nonmath = extra_compile_args_nonmath_optimized
    my_extra_link_args_math       = extra_link_args_math_optimized
    my_extra_link_args_nonmath    = extra_link_args_nonmath_optimized
    my_debug = True
    print( "build configuration selected: optimized" )

elif build_type == 'debug':
    my_extra_compile_args_math    = extra_compile_args_math_debug
    my_extra_compile_args_nonmath = extra_compile_args_nonmath_debug
    my_extra_link_args_math       = extra_link_args_math_debug
    my_extra_link_args_nonmath    = extra_link_args_nonmath_debug
    my_debug = True
    print( "build configuration selected: debug" )

else:
    raise ValueError("Unknown build configuration '%s'; valid: 'optimized', 'debug'" % (build_type))


def declare_cython_extension(extName, use_math=False, use_openmp=False):
    extPath = extName.replace(".", os.path.sep)+".pyx"
    if use_math:
        compile_args = list(my_extra_compile_args_math) # copy
        link_args    = list(my_extra_link_args_math)
        libraries    = ["m"]  # link libm; this is a list of library names without the "lib" prefix
    else:
        compile_args = list(my_extra_compile_args_nonmath)
        link_args    = list(my_extra_link_args_nonmath)
        libraries    = None  # value if no libraries, see setuptools.extension._Extension

    return Extension( extName,
                      [extPath],
                      extra_compile_args=compile_args,
                      extra_link_args=link_args,
                      libraries=libraries,
                      language="c++",
                    )
ext_module_mrf = declare_cython_extension( "FastMRFLayer", use_math=True, use_openmp=True )
ext_module_mem = declare_cython_extension( "memory", use_math=False, use_openmp=False )
cython_ext_modules = [ext_module_mrf]
my_ext_modules = cythonize( cython_ext_modules, include_path=my_include_dirs, gdb_debug=my_debug )

setup(
    setup_requires = ["cython"],
    install_requires = [],
    ext_modules = my_ext_modules,
    include_dirs=[numpy.get_include()]
)
from FastMRFLayer import FastMRFLayer
import tensorflow as tf

layer = FastMRFLayer(10, 2, 2, 100)
y = layer([tf.zeros([256, 256, 3], dtype=tf.int32), tf.zeros([256, 256, 3], dtype=tf.int32)])
print(y.numpy())
